Feature: Test for Auditors and Directors comparison

  @Regression
  Scenario Outline: Validation for Directors and Auditors Table members comparison
    When Select bank drop down with following value "<Bank Name>"
    When Click on Consult button
    When Click on Directors link and select members list auditors
    When Click on Auditors link
    Then Compare Auditors Table members and Director members "<Table Name>"

    Examples:
    |Bank Name|Table Name        |
    |00340    |External          |
    |00340    |Internal          |
    |00340    |Partners          |
    |00340    |Commit            |
