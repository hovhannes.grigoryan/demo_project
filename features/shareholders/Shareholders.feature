Feature: Test for Directors and Shareholders comparison

  @Regression
  Scenario Outline: Validation for Directors and Auditors Table members comparison
    When Select bank drop down with following value "<Bank Name>"
    When Click on Consult button
    When Click on Directors link and select members list shareholders
    When Click on Shareholders link
    Then Compare Shareholders Table members and Director members

    Examples:
      |Bank Name|
      |00432    |