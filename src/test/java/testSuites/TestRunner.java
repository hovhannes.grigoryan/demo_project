package testSuites;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(strict = true, features="features",
        glue={"hooks", "webDriver", "steps","pageElements"},
        tags={"@Regression"})

public class TestRunner {
}
