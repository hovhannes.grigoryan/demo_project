package steps.financialExecutionSteps;

import io.cucumber.java.en.When;
import org.openqa.selenium.support.ui.Select;
import pageElements.financialEntities.FinancialEntitiesPageElements;

public class FinancialExecutionSteps extends FinancialEntitiesPageElements {

    @When("^Click on Directors link$")
    public void clickOnDirectorsLink(){
        categories.get(0).click();
    }

    @When("^Click on Shareholders link$")
    public void clickOnShareholdersLink(){
        categories.get(1).click();
    }

    @When("^Click on Auditors link$")
    public void clickOnAuditorsLink(){
        categories.get(2).click();
    }

    @When("^Select bank drop down with following value \\\"([^\\\"]*)\\\"$")
    public void selectBankDropDown(String dropDownValue){
        Select banksDropDowns = new Select(banksDropDown);
        banksDropDowns.selectByValue(dropDownValue);
    }

    @When("^Click on Consult button$")
    public void clickOnConsultButton(){
        consultButton.click();
    }

    public void navigateBack(){
        driver.navigate().back();
    }

}
