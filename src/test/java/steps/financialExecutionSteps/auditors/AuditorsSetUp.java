package steps.financialExecutionSteps.auditors;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import steps.financialExecutionSteps.FinancialExecutionSteps;
import steps.financialExecutionSteps.directors.DirectorsExecutionSteps;

import java.util.ArrayList;
import java.util.List;

public class AuditorsSetUp {

    private FinancialExecutionSteps financialExecutionSteps = new FinancialExecutionSteps();
    private DirectorsExecutionSteps directorsExecutionSteps = new DirectorsExecutionSteps();
    public static List<String> directorsList = new ArrayList<>();

    @When("^Click on Directors link and select members list auditors$")
    public void clickOnDirectorsLinkAndSelectList(){
        financialExecutionSteps.clickOnDirectorsLink();
        directorsList = directorsExecutionSteps.getDirectorsList();
        financialExecutionSteps.navigateBack();
    }

}
