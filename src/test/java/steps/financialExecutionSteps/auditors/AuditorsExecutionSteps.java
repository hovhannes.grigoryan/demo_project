package steps.financialExecutionSteps.auditors;

import org.openqa.selenium.WebElement;
import pageElements.financialEntities.auditors.AuditorsPageElements;

import java.util.ArrayList;
import java.util.List;

public class AuditorsExecutionSteps extends AuditorsPageElements {

    public List<String> getExternalAuditorsTableList(){
        return getTableList(externalAuditors);
    }

    public List<String> getInternalAuditorsTableList(){
        return getTableList(internalAuditors);
    }

    public List<String> getResponsiblePartnersTableList(){
        return getTableList(responsiblePartners);
    }

    public List<String> getAuditCommitMembersTableList(){
        return getTableList(auditCommitMembers);
    }

    private List<String> getTableList(List<WebElement> table){
        List<String> tableMembersList = new ArrayList<>();
        for (int i = 0; i < table.size(); i++){
            tableMembersList.add(table.get(i).getText());
        }
        return tableMembersList;
    }

}
