package steps.financialExecutionSteps.auditors;

import io.cucumber.java.en.Then;

import java.util.List;

public class AuditorsValidationSteps {

    private AuditorsExecutionSteps auditorsExecutionSteps = new AuditorsExecutionSteps();

    @Then("^Compare Auditors Table members and Director members \\\"([^\\\"]*)\\\"$")
    public void compareAuditorTableMembersAndDirectorMembers(String tableName){
        switch (tableName){
            case "External":
                compareMembers(auditorsExecutionSteps.getExternalAuditorsTableList());
                break;
            case "Internal":
                compareMembers(auditorsExecutionSteps.getInternalAuditorsTableList());
                break;
            case "Partners":
                compareMembers(auditorsExecutionSteps.getResponsiblePartnersTableList());
                break;
            case "Commit":
                compareMembers(auditorsExecutionSteps.getAuditCommitMembersTableList());
                break;
        }
    }

    private void compareMembers(List<String> membersList){
        List<String> membersNames = membersList;
        String memberName;
        for (int i = 0; i < membersNames.size();i++){
            if (membersNames.get(i).contains(",")){
                memberName = membersNames.get(i).replace(",","");
            }else {
                memberName = membersNames.get(i);
            }
            for (int j = 0; j < AuditorsSetUp.directorsList.size();j++){
                if (membersNames.get(i).equals(AuditorsSetUp.directorsList.get(j))){
                    System.out.println(membersNames.get(i));
                }else if (memberName.replace(memberName.substring(memberName.lastIndexOf(" "),memberName.length() - 1),"").equals(AuditorsSetUp.directorsList.get(j))){
                    System.out.println(membersNames.get(i));
                }
            }
        }
    }

}
