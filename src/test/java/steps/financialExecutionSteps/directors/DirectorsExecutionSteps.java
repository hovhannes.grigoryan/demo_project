package steps.financialExecutionSteps.directors;

import pageElements.financialEntities.directors.DirectorsPageElements;

import java.util.ArrayList;
import java.util.List;

public class DirectorsExecutionSteps extends DirectorsPageElements {

    public List<String> getDirectorsList(){
        List<String> listOfDirectors = new ArrayList<>();
        for (int i = 0; i < directors.size();i++){
            listOfDirectors.add(directors.get(i).getText());
        }
        return listOfDirectors;
    }

}
