package steps.financialExecutionSteps.shareholders;

import io.cucumber.java.en.Then;

import java.util.List;

public class ShareholderValidationSteps {

    private ShareholdersExecutionSteps shareholdersExecutionSteps = new ShareholdersExecutionSteps();

    @Then("^Compare Shareholders Table members and Director members$")
    public void compareShareholdersTableMembersAndDirectorMembers(){
        List<String> shareholders = shareholdersExecutionSteps.getShareHoldersList();
        String member = null;
        for (int i = 0; i < ShareholdersSetUp.directorsList.size();i++){
            for (int j = 0; j < shareholders.size();j++){
                if (!shareholders.get(j).contains(",")){
                    if (shareholders.get(j).equals(ShareholdersSetUp.directorsList.get(i))){
                        System.out.println(ShareholdersSetUp.directorsList.get(i));
                    }
                }else {
                    member = shareholders.get(j).replace(",","");
                    if (member.equals(ShareholdersSetUp.directorsList.get(i))){
                        System.out.println(ShareholdersSetUp.directorsList.get(i));
                    }
                }
            }
        }
    }

}
