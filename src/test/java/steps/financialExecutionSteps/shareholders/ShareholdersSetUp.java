package steps.financialExecutionSteps.shareholders;

import io.cucumber.java.en.When;
import steps.financialExecutionSteps.FinancialExecutionSteps;
import steps.financialExecutionSteps.directors.DirectorsExecutionSteps;

import java.util.ArrayList;
import java.util.List;

public class ShareholdersSetUp {

    private FinancialExecutionSteps financialExecutionSteps = new FinancialExecutionSteps();
    private DirectorsExecutionSteps directorsExecutionSteps = new DirectorsExecutionSteps();
    public static List<String> directorsList = new ArrayList<>();

    @When("^Click on Directors link and select members list shareholders$")
    public void clickOnDirectorsLinkAndSelectListShareholders$(){
        financialExecutionSteps.clickOnDirectorsLink();
        directorsList = directorsExecutionSteps.getDirectorsList();
        financialExecutionSteps.navigateBack();
    }

}
