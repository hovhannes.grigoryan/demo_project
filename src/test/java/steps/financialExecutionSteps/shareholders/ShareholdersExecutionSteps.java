package steps.financialExecutionSteps.shareholders;

import pageElements.financialEntities.shareholders.ShareholdersPageElements;

import java.util.ArrayList;
import java.util.List;

public class ShareholdersExecutionSteps extends ShareholdersPageElements {

    public List<String> getShareHoldersList(){
        List<String> shareholdersList = new ArrayList<>();
        for (int i = 0; i < shareholders.size(); i++){
            shareholdersList.add(shareholders.get(i).getText());
        }
        return shareholdersList;
    }

}
