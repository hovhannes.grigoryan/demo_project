package pageElements.financialEntities.shareholders;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageElements.financialEntities.FinancialEntitiesPageElements;

import java.util.List;

public class ShareholdersPageElements extends FinancialEntitiesPageElements {

    @FindBy(css = "table[class='table table-BCRA table-bordered table-hover table-responsive'] tbody td:first-child")
    protected List<WebElement> shareholders;

}
