package pageElements.financialEntities.directors;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageElements.financialEntities.FinancialEntitiesPageElements;

import java.util.List;

public class DirectorsPageElements extends FinancialEntitiesPageElements {

    @FindBy(css = "table[class='table table-BCRA table-bordered table-hover table-responsive'] tbody tr td:last-child")
    protected List<WebElement> directors;

}
