package pageElements.financialEntities;

import hooks.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class FinancialEntitiesPageElements extends BasePage {

    @FindBy(css = "tr td a")
    protected List<WebElement> categories;

    @FindBy(css = "button[class='btn btn-primary btn-sm']")
    protected WebElement consultButton;

    @FindBy(css = "select[class='form-control']")
    protected WebElement banksDropDown;
}
