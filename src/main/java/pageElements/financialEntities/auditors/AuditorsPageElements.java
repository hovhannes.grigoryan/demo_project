package pageElements.financialEntities.auditors;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageElements.financialEntities.FinancialEntitiesPageElements;

import java.util.List;

public class AuditorsPageElements extends FinancialEntitiesPageElements {

    @FindBy(css = "table[class='table table-BCRA table-bordered table-hover table-responsive']:nth-child(3) tbody td:first-child")
    protected List<WebElement> externalAuditors;

    @FindBy(css = "table[class='table table-BCRA table-bordered table-hover table-responsive']:nth-child(4) tbody td:first-child")
    protected List<WebElement> internalAuditors;

    @FindBy(css = "table[class='table table-BCRA table-bordered table-hover table-responsive']:nth-child(5) tbody td:first-child")
    protected List<WebElement> responsiblePartners;

    @FindBy(css = "table[class='table table-BCRA table-bordered table-hover table-responsive']:nth-child(6) tbody td:last-child")
    protected List<WebElement> auditCommitMembers;

}
