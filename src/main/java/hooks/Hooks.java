package hooks;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Hooks extends BasePage {

    private String websiteUrl = "http://www.bcra.gov.ar/SistemasFinancierosYdePagos/Entidades_financieras.asp";

   @Before
   public void openBrowser(){
	   driver = new ChromeDriver();
	   driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	   driver.get(websiteUrl);
	   driver.manage().window().maximize();
   }

   @After
   public void tearDown(){
   	   driver.close();
	   driver.quit();
   }

}
