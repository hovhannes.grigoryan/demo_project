package hooks;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import webDriver.Driver;

public class BasePage extends Driver {

    public static WebDriver driver = getDriver();

    public BasePage(){
        PageFactory.initElements(driver, this);
    }

}
