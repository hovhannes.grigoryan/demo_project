package webDriver;

public class DriverInit {

    private static String path = "src/main/resources/drivers/chromedriverWin.exe";

    static void initDriver(){
        System.setProperty("webdriver.chrome.driver", path);
    }
}
