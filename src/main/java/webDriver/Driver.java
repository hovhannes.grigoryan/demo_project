package webDriver;

import org.openqa.selenium.WebDriver;

public class Driver {

    private static WebDriver webDriver;

    public Driver(){
        DriverInit.initDriver();
    }

    public static WebDriver getDriver() {
        return webDriver;
    }

}
